package fruits.repository;

import fruits.entity.Fruit;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface FruitRepository extends MongoRepository<Fruit, String> {
	Fruit findByName(String name);
}
