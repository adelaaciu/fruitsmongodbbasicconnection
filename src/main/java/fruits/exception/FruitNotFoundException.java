package fruits.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FruitNotFoundException extends RuntimeException {
	public FruitNotFoundException(String fruitName) {
		super("Fruit " + fruitName + " is not present in the current list!");
	}
}
