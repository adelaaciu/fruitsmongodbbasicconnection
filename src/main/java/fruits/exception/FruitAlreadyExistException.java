package fruits.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FruitAlreadyExistException extends RuntimeException {
	public FruitAlreadyExistException(String fruitName) {
		super("Fruit " + fruitName + " already exists in the current list!");
	}
}
